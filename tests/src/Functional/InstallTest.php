<?php

namespace Drupal\Tests\oai_pmh_harvester\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test whether we can install our module without breaking the site.
 *
 * @group living_word
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['oai_pmh_harvester'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Make sure the site still works.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testLoadFront() {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Log in');
  }

}
