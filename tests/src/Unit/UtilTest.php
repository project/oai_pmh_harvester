<?php

namespace Drupal\Tests\oai_pmh_harvester\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\oai_pmh_harvester\Util;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Util class.
 *
 * @coversDefaultClass \Drupal\oai_pmh_harvester\Util
 * @group oai_pmh_harvester
 */
class UtilTest extends UnitTestCase {

  /**
   * Test the `Util::interpretDateTime` function.
   *
   * @param string $value
   *   The argument for `Util::interpretDateTime`.
   * @param int|null $expectedUnixTimestamp
   *   If not null, the expected return value's unix timestamp.
   * @param string|null $expectedExceptionType
   *   If not null, the exception that should be thrown.
   *
   * @covers ::interpretDateTime
   * @dataProvider dataForInterpretDateTime
   */
  public function testInterpretDateTime(
    string $value,
    ?int $expectedUnixTimestamp,
    ?string $expectedExceptionType
  ) {
    try {
      $d = Util::interpretDateTime($value);
    }
    catch (\Exception $e) {
      if (!$expectedExceptionType) {
        $this->fail($e->getMessage());
      }
      $this->assertInstanceOf($expectedExceptionType, $e);
      return;
    }

    // The time zone must always be UTC.
    $this->assertEquals(0, $d->getOffset());

    $this->assertEquals($expectedUnixTimestamp, $d->getTimestamp());
  }

  /**
   * Data provider for `testInterpretDateTime`.
   */
  public function dataForInterpretDateTime(): array {
    return [
      "The correct OAI-PMH datetime format, with second granularity." => [
        "2021-01-01T13:00:00Z",
        1609506000,
        NULL,
      ],
      "The correct OAI-PMH datetime format, with day granularity." => [
        "2021-01-01",
        1609459200,
        NULL,
      ],
      "ISO8601 / ATOM, using '+00:00' instead of 'Z'." => [
        "2021-01-01T13:00:00+00:00",
        1609506000,
        NULL,
      ],
      "Using SQL date format, omitting the time zone." => [
        "2021-01-01 13:00:00",
        NULL,
        \InvalidArgumentException::class,
      ],
      "Garbage." => [
        "random stuff",
        NULL,
        \InvalidArgumentException::class,
      ],
    ];
  }

  /**
   * Test the `Util::utcRequestTime` function.
   */
  public function testUtcRequestTime() {
    // Mock the Drupal::time container.
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $mockTime = $this->createMock('Drupal\Component\Datetime\TimeInterface');
    $mockTime->method('getRequestTime')->willReturn(12345);
    $container->set('datetime.time', $mockTime);

    $d = Util::utcRequestTime();
    $this->assertEquals(0, $d->getOffset());
    $this->assertEquals(12345, $d->getTimestamp());
  }

}
