<?php

namespace Drupal\oai_pmh_harvester\Batch;

use Drupal\oai_pmh_harvester\Harvester;
use Drupal\oai_pmh_harvester\Util;
use Phpoaipmh\Endpoint;

/**
 * Batch functions for harvesting specific records.
 */
class HarvestSpecificRecordsBatch {

  /**
   * Start the batch job.
   *
   * @param int[] $ids
   *   An array of IDs to harvest.
   */
  public static function set(array $ids) {
    batch_set([
      'title' => t('Harvesting records'),
      'operations' => [
        [
          [self::class, 'process'],
          [$ids],
        ],
      ],
      'finished' => [
        self::class,
        'finished',
      ],
    ]);
  }

  /**
   * The process callback for this batch function.
   *
   * @param array $ids
   *   An array of IDs to harvest.
   * @param array|\ArrayAccess $context
   *   The batch context array.
   *
   * @throws \Exception
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_operation
   */
  public static function process(array $ids, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['n_completed'] = 0;
      $context['sandbox']['total'] = count($ids);
    }

    /**
     * @var int $limit
     *
     * A trade-off between speed, progress bar granularity and the risk of
     * hitting the PHP script execution timeout.
     */
    $limit = 5;

    $endpoint = Endpoint::build(Util::getOaiPmhUrl());
    $service = \Drupal::service('oai_pmh_harvester.harvester');
    $prefix = Util::getOaiPmhPrefix();

    foreach (array_slice($ids, $context['sandbox']['n_completed'], $limit) as $id) {
      // Prepend the OAI-PMH prefix if the ID does not already start with it.
      if (substr($id, 0, strlen($prefix)) !== $prefix) {
        $id = implode(":", [$prefix, $id]);
      }

      try {
        [$xml_id, $action] = $service->harvestOne($endpoint->getRecord($id, Harvester::FORMAT)->GetRecord->record);
        $context['results']['ids'][$action][] = $id;
      }
      catch (\Throwable $e) {
        // Don't allow errors to stop the batch process.
        // Save the errors and report them at the end.
        $context['results']['errors'][$id] = $e->getMessage();

        if (count($context['results']['errors']) > 100) {
          // Too many errors, so the problem is probably systemic, rather than
          // specific to the a few records.
          throw new \Exception("Too many errors. Aborting batch.");
        }
      }
      $context['sandbox']['n_completed']++;
    }

    $context['message'] = t("Harvested @completed of @total records.", [
      '@completed' => $context['sandbox']['n_completed'],
      '@total' => $context['sandbox']['total'],
    ]);
    if ($context['sandbox']['n_completed'] != $context['sandbox']['total']) {
      $context['finished'] = $context['sandbox']['n_completed'] / $context['sandbox']['total'];
    }
  }

  /**
   * Complete a batch process.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param mixed $results
   *   The value set in $context['results'] by process().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_finished
   */
  public static function finished(bool $success, $results, array $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $messenger->addMessage("Harvested records successfully.");
      foreach ($results['ids'] ?? [] as $action => $ids) {
        $messenger->addMessage("$action: " . implode(", ", $ids));
      }
    }
    else {
      $messenger->addError("An error occurred while trying to harvest records.");
    }

    foreach ($results['errors'] ?? [] as $id => $e) {
      $messenger->addError("Error while harvesting record $id: {$e}");
    }
  }

}
