<?php

namespace Drupal\oai_pmh_harvester\Batch;

use Drupal\oai_pmh_harvester\Util;
use Phpoaipmh\Endpoint;

/**
 * Batch functions for harvesting all records in a given date range.
 */
class HarvestRecordsInDateRangeBatch {

  /**
   * Start the batch job.
   *
   * @param \DateTimeImmutable|null $start
   *   The start of the harvesting range. If empty, it will harvest from the
   *   beginning.
   * @param \DateTimeImmutable|null $end
   *   The end of the harvesting range. If empty, it will harvest all the way
   *   to the end.
   */
  public static function set(\DateTimeImmutable $start = NULL, \DateTimeImmutable $end = NULL) {
    batch_set([
      'title' => t('Harvesting records'),
      'operations' => [
        [
          [self::class, 'process'],
          [$start, $end],
        ],
      ],
      'finished' => [
        self::class,
        'finished',
      ],
    ]);
  }

  /**
   * Update the progress bar and progress message.
   */
  private static function updateProgress(&$context): void {
    $n = $context['results']['n'];
    $token = $context['sandbox']['token'];

    $total = $context['results']['total'];
    $total_int = $total ?? PHP_INT_MAX;
    $total_string = $total ?? "unknown";

    if ($n < ($total_int)) {
      $context['finished'] = $n / ($total_int);
      $context['message'] = t("Harvested @n of @total records.<br>Deleted @count_deleted records.<br>Updated @count_updated records.<br>Resumption token is `@token`.", [
        '@n' => $n,
        '@total' => $total_string,
        '@count_deleted' => count($context['results']['ids']['deleted']),
        '@count_updated' => count($context['results']['ids']['updated']),
        '@token' => $token,
      ]);
    }
    else {
      $context['finished'] = 1;
    }
  }

  /**
   * The process callback for this batch function.
   *
   * @param \DateTimeImmutable|null $start
   *   The start of the harvesting range. If empty, it will harvest from the
   *   beginning.
   * @param \DateTimeImmutable|null $end
   *   The end of the harvesting range. If empty, it will harvest all the way
   *   to the end.
   * @param array|\ArrayAccess $context
   *   The batch context array.
   *
   * @throws \Exception
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_operation
   */
  public static function process(
    ?\DateTimeImmutable $start,
    ?\DateTimeImmutable $end,
    &$context
  ) {
    $endpoint = Endpoint::build(Util::getOaiPmhUrl());

    if (empty($context['sandbox'])) {
      // First run.
      $context['sandbox']['token'] = NULL;
      $context['results']['n'] = 0;
      $context['results']['total'] = 0;
      $context['results']['ids']['updated'] = [];
      $context['results']['ids']['deleted'] = [];
    }

    /** @var \Drupal\oai_pmh_harvester\Service\HarvesterService $record_processor */
    $record_processor = \Drupal::service('oai_pmh_harvester.harvester');
    $format = 'marcxml';

    /** @var string|null $token */
    $token = $context['sandbox']['token'];

    $iterator = $endpoint->listRecords($format, $start, $end, NULL, $token);
    $iterator->retrieveNextBatch();
    $batch = $iterator->getBatch();

    if (count($batch) < 1) {
      $context['finished'] = 1;
      return;
    }

    foreach ($batch as $xml) {
      /** @var \SimpleXMLElement $xml */
      [$id, $action] = $record_processor->harvestOne($xml);
      $context['results']['ids'][$action][] = $id;
      $context['results']['n']++;
    }

    $context['sandbox']['token'] = $iterator->getResumptionToken();
    $context['results']['total'] = $iterator->getTotalRecordCount();
    self::updateProgress($context);
  }

  /**
   * Complete a batch process.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param mixed $results
   *   The value set in $context['results'] by process().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_finished
   */
  public static function finished(bool $success, $results, array $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $messenger->addMessage(t("Harvested @count records.", [
        '@count' => $results['n'],
      ]));
      $messenger->addMessage(t("Deleted @count records: @ids", [
        '@count' => count($results['ids']['deleted']),
        '@ids' => implode(", ", $results['ids']['deleted']),
      ]));
      $messenger->addMessage(t("Updated @count records: @ids", [
        '@count' => count($results['ids']['updated']),
        '@ids' => implode(", ", $results['ids']['updated']),
      ]));
    }
    else {
      $messenger->addError("An error occurred while trying to harvest records.");
    }
  }

}
