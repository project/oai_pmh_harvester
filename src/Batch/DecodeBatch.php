<?php

namespace Drupal\oai_pmh_harvester\Batch;

use Drupal\Core\Database\Database;
use Drupal\oai_pmh_harvester\Util;

/**
 * Batch functions for decoding harvested records.
 */
class DecodeBatch {

  /**
   * Start the batch job.
   *
   * @param array $ids
   *   An array of IDs to decode. If empty, all records will be decoded.
   */
  public static function set(array $ids = []) {
    batch_set([
      'title' => t('Re-decoding harvested records'),
      'operations' => [
        [
          [self::class, 'process'],
          [$ids],
        ],
      ],
      'finished' => [
        self::class,
        'finished',
      ],
    ]);
  }

  /**
   * The process callback for this batch function.
   *
   * @param array $ids
   *   An array of IDs to decode. If empty, all records will be decoded.
   * @param array|\ArrayAccess $context
   *   The batch context array.
   *
   * @throws \Exception
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_operation
   */
  public static function process(array $ids, &$context) {
    $conn = Database::getConnection();

    if (empty($context['sandbox'])) {
      $context['sandbox']['n_completed'] = 0;
      $context['sandbox']['current_id'] = 0;
      $select = $conn->select('oai_pmh_harvester_bib_records', 'b');
      if (count($ids)) {
        // A list of IDs were specified, so only process those.
        $select->condition('id', $ids, 'IN');
      }
      $context['sandbox']['total'] = $select
        ->countQuery()
        ->execute()
        ->fetchField();
    }

    /**
     * @var int $limit
     *
     * A trade-off between speed, progress bar granularity and the risk of
     * hitting the PHP script execution timeout.
     */
    $limit = 500;

    $select = $conn->select('oai_pmh_harvester_bib_records', 'b')
      ->fields('b', [
        'id',
        'harvested_data',
      ])
      ->condition('id', $context['sandbox']['current_id'], '>');
    if (count($ids)) {
      // A list of IDs were specified, so only process those.
      $select->condition('id', $ids, 'IN');
    }
    $results = $select
      ->orderBy('id')
      ->range(0, $limit)
      ->execute();

    foreach ($results as $record) {
      try {
        self::processOne($record);
      }
      catch (\Throwable $e) {
        // Don't allow errors to stop the batch process.
        // Save the errors and report them at the end.
        $context['results']['errors'][$record->id] = $e->getMessage();

        if (count($context['results']['errors']) > 100) {
          // Too many errors, so the problem is probably systemic, rather than
          // specific to the a few records.
          throw new \Exception("Too many errors. Aborting batch.");
        }
      }
      $context['sandbox']['n_completed']++;
      $context['sandbox']['current_id'] = $record->id;
    }

    $context['message'] = "Decoded " . $context['sandbox']['n_completed'] . " of " . $context['sandbox']['total'] . " records.";
    if ($context['sandbox']['n_completed'] != $context['sandbox']['total']) {
      $context['finished'] = $context['sandbox']['n_completed'] / $context['sandbox']['total'];
    }
  }

  /**
   * Process one record. Decode it and update it in the database.
   *
   * @param object $record
   *   The record to re-decode.
   *
   * @throws \Scriptotek\Marc\Exceptions\RecordNotFound
   * @throws \Exception
   */
  private static function processOne(object $record): void {
    // Decode the data and extract the authors and titles.
    $xml = new \SimpleXMLElement($record->harvested_data);
    /** @var \Drupal\oai_pmh_harvester\Service\DecoderService $decoder */
    $decoder = \Drupal::service('oai_pmh_harvester.decoder');
    $csl = $decoder->decodeOne($xml->metadata->record);

    Database::getConnection()->update('oai_pmh_harvester_bib_records')
      ->fields([
        'decoded_time' => time(),
        'decoded_data' => json_encode($csl),
        'authors' => Util::getAuthorsIndexString($csl),
        'title' => Util::getTitleIndexString($csl),
      ])
      ->condition('id', $record->id)
      ->execute();
  }

  /**
   * Complete a batch process.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param mixed $results
   *   The value set in $context['results'] by process().
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_finished
   */
  public static function finished(bool $success, $results) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $messenger->addMessage("Re-decoded harvested records successfully.");
    }
    else {
      $messenger->addError("An error occurred while trying to re-decode harvested records.");
    }

    if (!empty($results['errors'])) {
      foreach ($results['errors'] as $id => $e) {
        $messenger->addError("Error while decoding record $id: $e");
      }
    }
  }

}
