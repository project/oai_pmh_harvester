<?php

namespace Drupal\oai_pmh_harvester\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class InspectForm.
 *
 * @package Drupal\oai_pmh_harvester\Form
 */
class InspectForm extends FormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * Inspect form constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\State\State $state
   *   The state.
   * @param \Drupal\Core\Database\Connection $db
   *   The database connection.
   */
  public function __construct(
    Messenger $messenger,
    State $state,
    Connection $db
  ) {
    $this->messenger = $messenger;
    $this->state = $state;
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('state'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inspect_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $ids = $form_state->get('ids') ?? [];

    $form['record_ids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Record IDs:'),
      '#description' => $this->t('Enter a comma-separated list of record IDs to inspect.'),
      '#default_value' => implode(",", $ids),
      '#pattern' => '[\d, ]+',
      '#required' => TRUE,
    ];

    $options = [];
    if (count($ids)) {
      $records = $this->db
        ->select('oai_pmh_harvester_bib_records', 'o')
        ->fields('o')
        ->condition('id', $ids, 'IN')
        ->orderBy('decoded_time', 'DESC')
        ->execute();

      foreach ($records as $record) {
        $marc = htmlspecialchars($record->harvested_data);
        $csl = htmlspecialchars(json_encode(json_decode($record->decoded_data), JSON_PRETTY_PRINT));
        $options[$record->id] = [
          'id' => $record->id,
          'oai_pmh_time' => $record->oai_pmh_time,
          'harvested_data' => [
            'data' => [
              '#markup' => "<pre>$marc</pre>",
            ],
          ],
          'decoded_time' => $record->decoded_time,
          'decoded_data' => [
            'data' => [
              '#markup' => "<pre>$csl</pre>",
            ],
          ],
          'authors' => [
            'data' => [
              '#markup' => str_replace("|", "<br>", $record->authors),
            ],
          ],
          'title' => [
            'data' => [
              '#markup' => str_replace("|", "<br>", $record->title),
            ],
          ],
        ];
      }
    }

    $form['records'] = [
      '#type' => 'tableselect',
      '#header' => [
        'id' => $this->t('ID'),
        'oai_pmh_time' => $this->t('OAI-PMH timestamp'),
        'harvested_data' => $this->t('Harvested MARC-XML'),
        'decoded_time' => $this->t('Decoded timestamp'),
        'decoded_data' => $this->t('Decoded CSL-JSON'),
        'authors' => $this->t('Authors'),
        'title' => $this->t('Titles'),
      ],
      '#options' => $options,
      '#empty' => $this->t('No records selected.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh table'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->set(
      'ids',
      array_map('intval', explode(",", $form_state->getValue('record_ids')))
    );
    $form_state->setRebuild(TRUE);
  }

}
