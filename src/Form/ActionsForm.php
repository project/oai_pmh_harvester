<?php

namespace Drupal\oai_pmh_harvester\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;
use Drupal\oai_pmh_harvester\Batch\DecodeBatch;
use Drupal\oai_pmh_harvester\Batch\HarvestRecordsInDateRangeBatch;
use Drupal\oai_pmh_harvester\Batch\HarvestSpecificRecordsBatch;
use Drupal\oai_pmh_harvester\Util;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function oai_pmh_harvester__run_harvest;

/**
 * A form providing administrative actions.
 *
 * @package Drupal\oai_pmh_harvester\Form
 */
class ActionsForm extends FormBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * Actions form constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\State\State $state
   *   The state.
   */
  public function __construct(
    Messenger $messenger,
    State $state
  ) {
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'actions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {
      /**
       * @var \DateTimeImmutable $harvest_from
       * @var \DateTimeImmutable $harvest_until
       */
      [$harvest_from, $harvest_until] = Util::getHarvester()->getHarvestRange();
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['action'] = [
      '#type' => 'radios',
      '#options' => [
        'reset_date' => $this->t('Reset the harvest date. This will cause all existing records to eventually be re-harvested during cron runs. Old records will still be available in the mean time.'),
        'harvest_cron' => $this->t(
          "Do one harvest run. This will harvest records from @from until @until.",
          [
            '@from' => $harvest_from->format(\DateTimeInterface::ATOM),
            '@until' => $harvest_until->format(\DateTimeInterface::ATOM),
          ]
        ),
        'harvest_remaining' => $this->t(
          'Harvest all remaining records. This will harvest everything from @from to date.',
          ['@from' => $harvest_from->format(\DateTimeInterface::ATOM)]
        ),
        'harvest_some' => $this->t('(Re-)harvest specific records, listed below. The harvest date is not taken into account.'),
        'redecode_all' => $this->t('Re-decode all harvested records. This will not re-harvest records.'),
        'redecode_some' => $this->t('Re-decode specific harvested records, listed below. This will not re-harvest records.'),
      ],
      '#title' => $this->t('Actions'),
    ];
    $form['actions']['record_ids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Record IDs:'),
      '#description' => $this->t('Enter a comma-separated list of record IDs to harvest or re-decode.'),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Do it!'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $record_ids = array_map(
      'intval',
      explode(",", $form_state->getValue('record_ids'))
    );

    switch ($form_state->getValue('action')) {
      case 'reset_date':
        $this->state->set('oai_pmh_harvester_last', NULL);
        $this->messenger->addMessage("Reset the harvest date.");
        break;

      case 'harvest_remaining':
        // Start a batch job to harvest all remaining records.
        $end = (new \DateTimeImmutable("now"))->setTimezone(new \DateTimeZone('UTC'));
        HarvestRecordsInDateRangeBatch::set(\Drupal::state()->get('oai_pmh_harvester_last'), $end);
        \Drupal::state()->set('oai_pmh_harvester_last', $end);
        break;

      case 'harvest_some':
        // Start a batch job to harvest all specified records.
        HarvestSpecificRecordsBatch::set($record_ids);
        break;

      case 'redecode_all':
        // Start a batch job to re-decode all harvested records.
        DecodeBatch::set();
        break;

      case 'redecode_some':
        // Start a batch job to re-decode the specified harvested records.
        DecodeBatch::set($record_ids);
        break;

      case 'harvest_cron':
        try {
          oai_pmh_harvester__run_harvest();
        }
        catch (\Exception $e) {
          $this->messenger->addMessage(
            "Harvest cron failed. {$e->getMessage()}",
            $this->messenger::TYPE_ERROR,
            FALSE
          );
        }
        break;

      case NULL:
        $this->messenger->addMessage("No action selected.");
    }
  }

}
