<?php

namespace Drupal\oai_pmh_harvester\Form;

use Drupal\auto_config_form\AutoConfigFormBase;

/**
 * A form for all config in this module.
 *
 * @package Drupal\oai_pmh_harvester\Form
 */
class ConfigForm extends AutoConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getSchemaKey(): string {
    return 'oai_pmh_harvester.settings';
  }

}
