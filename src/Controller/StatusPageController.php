<?php

namespace Drupal\oai_pmh_harvester\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\State;
use Drupal\oai_pmh_harvester\Util;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the status page.
 */
class StatusPageController extends ControllerBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $db;

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   */
  public static function create(ContainerInterface $container): StatusPageController {
    $instance = new static();
    $instance->state = $container->get('state');
    $instance->db = $container->get('database');
    return $instance;
  }

  /**
   * Callback for the status page.
   *
   * @return array
   *   Renderable status page.
   */
  public function status(): array {
    try {
      $content = [];

      $n_harvested = $this->db->select('oai_pmh_harvester_bib_records', 'b')
        ->countQuery()
        ->execute()
        ->fetchField();

      $n_not_decoded = $this->db->select('oai_pmh_harvester_bib_records', 'b')
        ->condition('decoded_time', NULL, 'IS NULL')
        ->countQuery()
        ->execute()
        ->fetchField();

      $n_outdated = $this->db->select('oai_pmh_harvester_bib_records', 'b')
        ->where("decoded_time < oai_pmh_time")
        ->countQuery()
        ->execute()
        ->fetchField();

      $content['info'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => [
          'harvested' => [
            '#type' => 'markup',
            '#markup' => $this->formatPlural(
              $n_harvested,
              'There is one harvested record.',
              'There are @count harvested records.'
            ),
          ],
          'not_decoded' => [
            '#type' => 'markup',
            '#markup' => $this->formatPlural(
              $n_not_decoded,
              'There is one record that is not yet decoded.',
              'There are @count records that are not yet decoded.'
            ),
          ],
          'outdated' => [
            '#type' => 'markup',
            '#markup' => $this->formatPlural(
              $n_outdated,
              'There is one record that was re-harvested since we decoded it.',
              'There are @count records that were re-harvested since we decoded it.'
            ),
          ],
        ],
      ];

      /**
       * @var \DateTimeImmutable $harvest_from
       * @var \DateTimeImmutable $harvest_until
       */
      [$harvest_from, $harvest_until] = Util::getHarvester()->getHarvestRange();
      $content['info']['#items']['time'] = [
        '#type' => 'markup',
        '#markup' => $this->t(
          'On the next cron, records from @from until @until will be harvested.',
          [
            '@from' => $harvest_from->format(\DateTimeInterface::ATOM),
            '@until' => $harvest_until->format(\DateTimeInterface::ATOM),
          ]
        ),
      ];
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return $content;
  }

}
