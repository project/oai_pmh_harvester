<?php

namespace Drupal\oai_pmh_harvester;

use Drupal\oai_pmh_harvester\Service\HarvesterService;
use Phpoaipmh\Endpoint;
use Phpoaipmh\Exception\OaipmhException;

/**
 * Harvests records.
 */
class Harvester {

  public const FORMAT = 'marcxml';

  /**
   * The URL of the OAI-PMH endpoint.
   *
   * @var string
   */
  private string $url;

  /**
   * The harvesting interval.
   *
   * This determines the maximum time range that is harvested per run.
   *
   * @var \DateInterval
   */
  private \DateInterval $interval;

  /**
   * The end ("until" value) of the last harvest run's range.
   *
   * @var \DateTimeImmutable|null
   */
  private ?\DateTimeImmutable $last;

  /**
   * The harvester service.
   *
   * Maybe this is a misnomer. Anyway, it parses the harvested XML results and
   * writes them to the database.
   *
   * @var \Drupal\oai_pmh_harvester\Service\HarvesterService
   */
  private HarvesterService $service;

  /**
   * The OAI-PMH endpoint.
   *
   * @var \Phpoaipmh\Endpoint
   */
  private Endpoint $endpoint;

  /**
   * Harvester constructor.
   *
   * @param string $url
   *   The URL of the OAI-PMH endpoint.
   * @param \DateInterval $interval
   *   The harvesting interval.
   *   This determines the maximum time range that is harvested per run.
   * @param \DateTimeImmutable|null $last
   *   The end ("until" value) of the last harvest run's range.
   *   Null if the harvester should start from the very beginning.
   * @param \Drupal\oai_pmh_harvester\Service\HarvesterService $service
   *   The harvester service. Maybe this is a misnomer. Anyway, it parses the
   *   harvested XML results and writes them to the database.
   */
  public function __construct(
    string $url,
    \DateInterval $interval,
    ?\DateTimeImmutable $last,
    HarvesterService $service
  ) {
    $this->url = $url;
    $this->interval = $interval;
    $this->last = $last;
    $this->service = $service;

    try {
      $this->endpoint = Endpoint::build($this->url);
    }
    catch (\Exception $e) {
      throw new \InvalidArgumentException(
        "Invalid OAI-PMH URL.",
        $e->getCode(),
        $e
      );
    }
  }

  /**
   * Write log messages about a harvest run.
   *
   * @param int $n
   *   The number of records harvested.
   * @param \DateTimeImmutable $harvest_from
   *   The harvest start time.
   * @param \DateTimeImmutable $harvest_until
   *   The harvest end time.
   * @param array $deleted
   *   The IDs of the deleted records.
   * @param array $updated
   *   The IDs of the updated records.
   */
  private static function log(int $n, \DateTimeImmutable $harvest_from, \DateTimeImmutable $harvest_until, array $deleted, array $updated): void {
    $logger = \Drupal::logger('oai_pmh_harvester');
    $logger->info(\Drupal::translation()->formatPlural(
      $n,
      'Harvested one record from @from until @until.',
      'Harvested @count records from @from until @until.',
      [
        '@from' => $harvest_from->format(\DateTimeInterface::ATOM),
        '@until' => $harvest_until->format(\DateTimeInterface::ATOM),
      ]
    ));
    if (count($deleted)) {
      $logger->debug(t("Deleted bibliographic records: [@list]", [
        '@list' => implode(", ", $deleted),
      ]));
    }
    if (count($updated)) {
      $logger->debug(t("Updated bibliographic records: [@list]", [
        '@list' => implode(", ", $updated),
      ]));
    }
  }

  /**
   * Perform one harvest run.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\DatabaseException
   * @throws \Drupal\oai_pmh_harvester\Exceptions\EndpointException
   * @throws \Phpoaipmh\Exception\OaipmhException
   * @throws \Scriptotek\Marc\Exceptions\RecordNotFound
   */
  public function run() {
    [$harvest_from, $harvest_until] = $this->getHarvestRange();
    $records_iterator = $this->endpoint->listRecords(
      self::FORMAT,
      $harvest_from,
      $harvest_until
    );

    $n = 0;
    $ids = [
      'updated' => [],
      'deleted' => [],
    ];
    try {
      foreach ($records_iterator as $xml) {
        /** @var \SimpleXMLElement $xml */
        [$id, $action] = $this->service->harvestOne($xml);
        $ids[$action][] = $id;
        $n++;
      }
    }
    catch (OaipmhException $e) {
      if ($e->getOaiErrorCode() != 'noRecordsMatch') {
        // We can't handle this exception. Log and rethrow.
        watchdog_exception('oai_pmh_harvester', $e);
        throw $e;
      }

      // We can safely ignore this exception!
      // We will query the next interval on the next run.
    } finally {
      // This runs even if we re-throw the exception inside the catch block.
      self::log(
        $n,
        $harvest_from,
        $harvest_until,
        $ids['deleted'],
        $ids['updated'],
      );
    }

    // This only runs if all exceptions were caught. This means that we don't
    // advance the harvest date if an error has occurred.
    $this->last = $harvest_until;
  }

  /**
   * Get the interval that should be harvested next.
   *
   * @return \DateTimeImmutable[]
   *   An array of DateTimeImmutable objects: [$harvest_from, $harvest_until].
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\EndpointException
   */
  public function getHarvestRange(): array {
    // If harvest_from is NULL, get the earliestDatestamp from "Identify".
    // See https://www.openarchives.org/OAI/openarchivesprotocol.html#Identify
    $harvest_from = $this->last ?? $this->getEarliestDatestamp();
    $harvest_until = $harvest_from->add($this->interval);

    // Don't allow the harvest period to go past the current time.
    $now = Util::utcRequestTime();
    if ($harvest_until > $now) {
      $harvest_until = $now;
    }

    // Don't allow the harvest period to be negative.
    if ($harvest_from > $harvest_until) {
      $harvest_from = $harvest_until;
    }

    return [
      $harvest_from,
      $harvest_until,
    ];
  }

  /**
   * Get the datestamp of the first record of the OAI-PMH provider.
   *
   * @return \DateTimeImmutable
   *   The datestamp of the first record of the OAI-PMH provider.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\EndpointException
   */
  public function getEarliestDatestamp(): \DateTimeImmutable {
    return Util::getEarliestDatestamp($this->endpoint);
  }

  /**
   * Get the datestamp of the last harvested record.
   *
   * @return \DateTimeImmutable|null
   *   The datestamp of the last harvested record.
   */
  public function getLast(): ?\DateTimeImmutable {
    return $this->last;
  }

}
