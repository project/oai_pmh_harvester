<?php

namespace Drupal\oai_pmh_harvester\Service;

use Drupal\oai_pmh_harvester\Event\HarvestPreMergeEvent;
use RudolfByker\PhpMarcCsl\MarcCslVariables;
use Scriptotek\Marc\Record;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The Decoder service.
 */
class DecoderService {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  public $dispatcher;

  /**
   * DecoderService constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function __construct(EventDispatcherInterface $dispatcher) {
    $this->dispatcher = $dispatcher;
  }

  /**
   * Decode one Marc XML record to CSL.
   *
   * @param \SimpleXMLElement $xml
   *   The Marc XML record to decode.
   *
   * @return \RudolfByker\PhpMarcCsl\MarcCslVariables
   *   The decoded CSL data.
   *
   * @throws \Scriptotek\Marc\Exceptions\RecordNotFound
   *   When the provided XML element is not a MARC record.
   */
  public function decodeOne(\SimpleXMLElement $xml): MarcCslVariables {
    $csl = new MarcCslVariables(Record::fromSimpleXMLElement($xml));

    // Allow other modules to modify the $csl object before we return it.
    $this->dispatcher->dispatch(
      new HarvestPreMergeEvent($xml, $csl),
      HarvestPreMergeEvent::EVENT_NAME
    );

    return $csl;
  }

}
