<?php

namespace Drupal\oai_pmh_harvester\Service;

use Drupal\Core\Database\Connection;
use Drupal\oai_pmh_harvester\Exceptions\DatabaseException;
use Drupal\oai_pmh_harvester\Util;
use Exception;
use SimpleXMLElement;

/**
 * The Harvester service.
 */
class HarvesterService {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  public Connection $db;

  /**
   * The Marc to CSL decoder service.
   *
   * @var \Drupal\oai_pmh_harvester\Service\DecoderService
   */
  public DecoderService $decoder;

  /**
   * HarvesterService constructor.
   *
   * @param \Drupal\Core\Database\Connection $db
   *   The database connection to use.
   * @param \Drupal\oai_pmh_harvester\Service\DecoderService $decoder
   *   The Marc to CSL decoder service.
   */
  public function __construct(Connection $db, DecoderService $decoder) {
    $this->db = $db;
    $this->decoder = $decoder;
  }

  /**
   * Harvest one XML record from an OAI-PMH source.
   *
   * @param \SimpleXMLElement $xml
   *   The XML record being harvested.
   *
   * @return array
   *   Multiple values: [id, action]
   *   Action is one of "deleted" or "updated".
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\DatabaseException
   *   When the provided XML element does not contain a MARC record.
   * @throws \Scriptotek\Marc\Exceptions\RecordNotFound
   *   When the DB merge query fails.
   */
  public function harvestOne(SimpleXMLElement $xml): array {
    $header = (array) $xml->header;
    $id = (int) explode(':', $header['identifier'])[1];

    if (isset($header['@attributes']['status']) && 'deleted' == $header['@attributes']['status']) {
      // Don't try to decode deleted records. Delete them from our table.
      $this->db->delete('oai_pmh_harvester_bib_records')
        ->condition('id', $id)
        ->execute();
      return [$id, "deleted"];
    }

    /*
     * Remove 505 from the XML record, since it tends to be so HUGE for some
     * books, that the harvested data is too big for the TEXT column. We
     * don't need 505.
     */

    // To access things inside the record, we need to register the namespace.
    $xml->registerXPathNamespace('m', 'http://www.loc.gov/MARC21/slim');

    // Get all 505.
    $tags_505 = $xml->xpath('//m:datafield[@tag="505"]');

    // Remove each one from the XML document.
    foreach ($tags_505 as $tag) {
      // See https://stackoverflow.com/a/4137027/836995
      unset($tag[0]);
    }

    // Decode the record.
    $csl = $this->decoder->decodeOne($xml->metadata->record);
    $oai_pmh_time = Util::interpretDateTime($header['datestamp']);

    $fields = [
      'oai_pmh_time' => $oai_pmh_time->getTimestamp(),
      'harvested_data' => $xml->asXML(),
      'decoded_time' => time(),
      'decoded_data' => json_encode($csl),
      'authors' => Util::getAuthorsIndexString($csl),
      'title' => Util::getTitleIndexString($csl),
    ];

    // Merge (i.e. update or insert) data into our table.
    try {
      $this->db->merge('oai_pmh_harvester_bib_records')
        ->key('id', $id)
        ->fields($fields)
        ->execute();
    }
    catch (Exception $e) {
      throw new DatabaseException(
        "Failed to save newly harvested records to the database: {$e->getMessage()}",
        $e->getCode(),
        $e
      );
    }

    return [$id, "updated"];
  }

}
