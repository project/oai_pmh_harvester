<?php

namespace Drupal\oai_pmh_harvester\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * An event that should fire just before harvested data is merged into the DB.
 */
class HarvestPreMergeEvent extends Event {

  const EVENT_NAME = 'oai_pmh_harvester_harvest_pre_merge';

  /**
   * The harvested OAI-PMH record.
   *
   * @var \SimpleXMLElement
   */
  public $xml;

  /**
   * The decoded CSL data.
   *
   * @var object
   */
  public $csl;

  /**
   * HarvestPreMergeEvent constructor.
   *
   * @param \SimpleXMLElement $xml
   *   The harvested OAI-PMH record.
   * @param object $csl
   *   The decoded CSL data.
   */
  public function __construct(\SimpleXMLElement $xml, object $csl) {
    $this->xml = $xml;
    $this->csl = $csl;
  }

}
