<?php

namespace Drupal\oai_pmh_harvester\Exceptions;

use RuntimeException;

/**
 * Throw this when the OAI-PMH endpoint has an error or returns invalid data.
 */
class EndpointException extends RuntimeException {}
