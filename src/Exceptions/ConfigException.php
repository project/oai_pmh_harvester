<?php

namespace Drupal\oai_pmh_harvester\Exceptions;

use RuntimeException;

/**
 * Throw this when the current configuration prevents the module from working.
 */
class ConfigException extends RuntimeException {}
