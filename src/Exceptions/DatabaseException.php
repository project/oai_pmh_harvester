<?php

namespace Drupal\oai_pmh_harvester\Exceptions;

use RuntimeException;

/**
 * Throw this when we fail to save data to or load data from our own tables.
 */
class DatabaseException extends RuntimeException {}
