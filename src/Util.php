<?php

namespace Drupal\oai_pmh_harvester;

use Drupal\Core\Database\Database;
use Drupal\oai_pmh_harvester\Exceptions\ConfigException;
use Drupal\oai_pmh_harvester\Exceptions\EndpointException;
use Phpoaipmh\Endpoint;
use RudolfByker\PhpMarcCsl\MarcCslVariables;

/**
 * Utility class for static functions.
 *
 * @package Drupal\oai_pmh_harvester
 */
class Util {

  const SETTINGS_KEY = 'oai_pmh_harvester.settings';

  /**
   * Check if any of the array elements (strings) contain the given string.
   *
   * @param string $needle
   *   The string to search for.
   * @param array $haystack
   *   The array of strings to search through.
   *
   * @return bool
   *   TRUE if at least one array element contains the string.
   */
  public static function findStringInArray(string $needle, array $haystack) {
    foreach ($haystack as $value) {
      if (FALSE !== strpos($value, $needle)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Removes duplicate elements from string arrays.
   *
   * Example:
   * - Input: ["Schutte, G. J. (Gerrit Jan), 1940-", "Schutte, G. J."]
   * - Output: ["Schutte, G. J. (Gerrit Jan), 1940-"]
   *
   * @param string[] $array
   *   The array of strings to deduplicate.
   *
   * @return string[]
   *   A new array of strings, sorted from longest to shortest, with duplicates
   *   removed.
   */
  public static function deduplicateStringArray(array $array): array {
    // First make sure the array of strings is sorted longest to shortest.
    usort($array, function ($a, $b) {
      return strlen($b) - strlen($a);
    });

    $new_values = [];
    foreach ($array as $value) {
      if (!empty($value)) {
        // Only add the value if it doesn't appear in one of the existing
        // values.
        if (!Util::findStringInArray($value, $new_values)) {
          $new_values[] = $value;
        }
      }
    }

    return $new_values;
  }

  /**
   * Get the string to store in the "title" column for searching.
   *
   * Concatenates the title, container title and collection title.
   *
   * @param \RudolfByker\PhpMarcCsl\MarcCslVariables $csl
   *   The Marc-CSL variables object.
   *
   * @return string
   *   Concatenated titles.
   */
  public static function getTitleIndexString(MarcCslVariables $csl): string {
    // 255 is the maximum string length for the title column.
    return mb_strimwidth(implode(" | ", Util::deduplicateStringArray(array_filter([
      $csl->getTitle(),
      $csl->getContainerTitle(),
      $csl->getCollectionTitle(),
    ]))), 0, 255);
  }

  /**
   * Get the string to store in the "authors" column for searching.
   *
   * Concatenates all authors from the given CSL Variables.
   *
   * @param \RudolfByker\PhpMarcCsl\MarcCslVariables $csl
   *   The Marc-CSL variables object.
   *
   * @return string
   *   Concatenated authors.
   */
  public static function getAuthorsIndexString(MarcCslVariables $csl): string {
    // 255 is the maximum string length for the authors column.
    return mb_strimwidth(implode(' | ', Util::deduplicateStringArray(array_map(function ($author) {
      return $author['literal'] ?? implode(" ", array_filter([
        $author['dropping-particle'] ?? "",
        $author['given'] ?? "",
        $author['family'] ?? "",
        $author['suffix'] ?? "",
      ]));
    }, $csl->getAuthor()))), 0, 255);
  }

  /**
   * Search the harvested records for a the given title and author.
   *
   * If title or author is a string with more than one word (i.e. containing
   * white space), it is broken up into separate words. Only records containing
   * all of those words are returned.
   *
   * @param string|null $title
   *   The title to search for. If NULL, we won't filter by title.
   * @param string|null $author
   *   The author to search for. If NULL, we won't filter by author.
   * @param int $limit
   *   The maximum number of results to return.
   *
   * @return array
   *   An array of bibliographic records, keyed by ID.
   */
  public static function search(string $title = NULL, string $author = NULL, int $limit = 10): array {
    $conn = Database::getConnection();
    $q = $conn->select('oai_pmh_harvester_bib_records', 'b')
      ->fields('b', [
        'decoded_data',
        'id',
      ]);
    if ($author) {
      // Split search term into separate words. Select for records that
      // contain every word, but not in any specific order.
      $author_words = preg_split('/\s+/', $author);
      foreach ($author_words as $word) {
        $q->condition('authors', '%' . $conn->escapeLike($word) . '%', 'LIKE');
      }
    }
    if ($title) {
      // Split search term into separate words. Select for records that
      // contain every word, but not in any specific order.
      $title_words = preg_split('/\s+/', $title);
      foreach ($title_words as $word) {
        $q->condition('title', '%' . $conn->escapeLike($word) . '%', 'LIKE');
      }
    }
    // @todo what is a good sort order for this query?
    $q->range(0, $limit);
    $references = [];
    foreach ($q->execute() as $result) {
      $references[(int) $result->id] = json_decode($result->decoded_data);
    }
    return $references;
  }

  /**
   * Interpret a date/time value obtained from OAI-PMH.
   *
   * Every date and time from OAI-PMH is supposed to be in UTC time zone and
   * formatted as 'YYYY-MM-DDThh:mm:ssZ' (for second granularity) or
   * 'YYYY-MM-DD' (for day granularity), but we'll be lenient and additionally
   * allow any value in ISO8601 format (called 'ATOM' in PHP).
   *
   * @param string $value
   *   The value to interpret.
   *
   * @return \DateTimeImmutable
   *   The return value will always be in UTC.
   *
   * @see http://www.openarchives.org/OAI/openarchivesprotocol.html#Dates
   * @see https://www.php.net/manual/en/class.datetimeinterface.php
   */
  public static function interpretDateTime(string $value): \DateTimeImmutable {
    try {
      $result = \DateTimeImmutable::createFromFormat(
        \DateTimeInterface::ATOM,
        $value,
        new \DateTimeZone('UTC')
      );

      if (!$result) {
        $result = \DateTimeImmutable::createFromFormat(
          'Y-m-d',
          $value,
          new \DateTimeZone('UTC')
        );
        if ($result) {
          // This is required because `createFromFormat` will use the current
          // time instead of midnight when the time is omitted in the input
          // string.
          $result = $result->setTime(0, 0);
        }
      }

      if (!$result) {
        // This is required because `createFromFormat` may return `false`.
        throw new \InvalidArgumentException("Failed to parse date/time value.");
      }

      return $result;
    }
    catch (\Throwable $e) {
      throw new \InvalidArgumentException("Error while trying to parse date/time value.", $e->getCode(), $e);
    }
  }

  /**
   * Get the time of the current request in UTC.
   *
   * @return \DateTimeImmutable
   *   The return value will always be in UTC.
   */
  public static function utcRequestTime(): \DateTimeImmutable {
    return (new \DateTimeImmutable())
      ->setTimestamp(\Drupal::time()->getRequestTime())
      ->setTimezone(new \DateTimeZone('UTC'));
  }

  /**
   * Get the harvest interval from config.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\ConfigException
   */
  public static function getHarvestInterval(): \DateInterval {
    $config_name = self::SETTINGS_KEY;
    $config_var_name = 'harvest_interval';

    $config = \Drupal::config($config_name);
    $interval_spec = $config->get($config_var_name);
    if (empty($interval_spec)) {
      throw new ConfigException("Missing configuration: '$config_name.$config_var_name'.");
    }
    try {
      $interval = new \DateInterval($interval_spec);
    }
    catch (\Exception $e) {
      throw new ConfigException(
        "Invalid configuration: '$config_name.$config_var_name'.",
        $e->getCode(),
        $e
          );
    }
    return $interval;
  }

  /**
   * Get the URL for the OAI-PMH endpoint from config.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\ConfigException
   */
  public static function getOaiPmhUrl(): string {
    $config_name = self::SETTINGS_KEY;
    $config_var_name = 'oai_url';

    $config = \Drupal::config($config_name);
    $url = $config->get($config_var_name);
    if (empty($url)) {
      throw new ConfigException("Missing configuration: '$config_name.$config_var_name'.");
    }
    return $url;
  }

  /**
   * Get the prefix for OAI-PMH IDs from the config.
   */
  public static function getOaiPmhPrefix(): string {
    return \Drupal::config(self::SETTINGS_KEY)->get('oai_prefix') ?? '';
  }

  /**
   * Get an OAI-PMH harvester, based on the current settings.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\ConfigException
   */
  public static function getHarvester(): Harvester {
    return new Harvester(
      Util::getOaiPmhUrl(),
      Util::getHarvestInterval(),
      \Drupal::state()->get('oai_pmh_harvester_last', NULL),
      \Drupal::service('oai_pmh_harvester.harvester')
    );
  }

  /**
   * Get the datestamp of the first record of the OAI-PMH provider.
   *
   * @return \DateTimeImmutable
   *   The datestamp of the first record of the OAI-PMH provider.
   *
   * @throws \Drupal\oai_pmh_harvester\Exceptions\EndpointException
   */
  public static function getEarliestDatestamp(Endpoint $endpoint): \DateTimeImmutable {
    $identify = $endpoint->identify();
    $value = $identify->Identify->earliestDatestamp;
    try {
      return Util::interpretDateTime($value);
    }
    catch (\InvalidArgumentException $e) {
      throw new EndpointException(
        "Failed to parse earliestDatestamp provided by OAI-PMH endpoint.",
        $e->getCode(),
        $e
          );
    }
  }

}
