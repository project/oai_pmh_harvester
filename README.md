# OAI-PMH Harvester

See https://www.drupal.org/project/oai_pmh_harvester

Harvest bibliographic records from an OAI-PMH source, like Koha, and cache them in a table, so that we can refer to them from fields.

This module does not provide the FieldType, FieldWidget or FieldFormatter plugins for working with the data.
That would be the job of a separate module depending on this one.

## Installing

Add the module to your Drupal project and enable it.

```
composer require drupal/oai_pmh_harvester
drush en oai_pmh_harvester
```

## Using

- Navigate to Admin -> Config -> Web services -> OAI-PMH Harvester -> Settings
(`admin/config/services/oai_pmh_harvester/settings_form`).
- Enter the URL to your OAI-PMH provider. For a Koha site, this is something like
`https://example.com/cgi-bin/koha/oai.pl`
- Choose a harvest interval. Since OAI-PMH provides the records in the order that they were added, this will determine
roughly how many records we try to harvest per cron run.
- Save configuration
- Make sure cron runs regularly, or run it manually from the actions page
(`admin/config/services/oai_pmh_harvester/actions_form`).
- A table called `oai_pmh_harvester_bib_records` will now exist in your database, and it will get filled with
bibliographic records harvested from your OAI-PMH endpoint.
- Look at the table column comments for further guidance and do with the data what you wish.
